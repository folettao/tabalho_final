# Aplicativo de persistência em Banco de Dados

Information is a primary resource for decision making, and it is with
it that a solution can be found. It can be stored by various means, always in
the form of data, digital spreadsheets, paper drafts, notepad on mobile
devices, etc., or in a database. In this way, it is possible to guarantee
scalability, transparency, performance, availability, among other “services”
that a database can offer. And through a DBMS (database management
system), the goal is to provide a pleasant experience to the user when the goal
is to manipulate their data, transforming it into information. Through this, we
have a simple DBMS conceived through the Java programming language,
which is integrated - through a jdbc (java database connectivity) library - to a
Structured Query Language (SQL) database.
